/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zurich University of                       -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ------------------------------------------------------------------------- */
/**
 *  \brief  Implementation of module threads_preemtive.
 * 
 *  $Id: threads_preemptive.c 1137 2016-12-12 09:08:49Z feur $
 * ------------------------------------------------------------------------- */


/* Standard includes */ 
#include <cmsis_os.h>
#include <hal_gpio.h>
#include <stdio.h>

/* User includes */
#include "threads.h"
#include "uart.h"


/* -- Macros
 * ------------------------------------------------------------------------- */

#define LED_GREEN           (0x1 << 13u)
#define LED_RED             (0x1 << 14u)
#define HALF_SECOND         (0x7fffff)


/* -- Variables with global scope
 * ------------------------------------------------------------------------- */


/* -- Local function declarations
 * ------------------------------------------------------------------------- */

static void wait_blocking(uint32_t value);


/* -- Thread definition
 * ------------------------------------------------------------------------- */

void thread1(void const *arg) {
    while (1) {
        hal_gpio_bit_set(GPIOG, LED_GREEN);
        wait_blocking(HALF_SECOND);
        hal_gpio_bit_reset(GPIOG, LED_GREEN);
        wait_blocking(HALF_SECOND);
    }
}

osThreadDef(thread1, osPriorityNormal, 1, 0);

void thread2(void const *arg) {
    while (1) {
        hal_gpio_bit_set(GPIOG, LED_RED);
        wait_blocking(HALF_SECOND);
        hal_gpio_bit_reset(GPIOG, LED_RED);
        wait_blocking(HALF_SECOND);
        osDelay(40u);
    }
}

osThreadDef(thread2, osPriorityAboveNormal, 1, 0);


/* -- Public function definitions
 * ------------------------------------------------------------------------- */

/*
 * See header file
 */
void threads_init(void)
{
    hal_gpio_output_t gpio;
    
    GPIOG_ENABLE();
  
    uart_init();
    
    gpio.pins = LED_GREEN | LED_RED;
    gpio.pupd = HAL_GPIO_PUPD_UP;
    gpio.out_speed = HAL_GPIO_OUT_SPEED_100MHZ;
    gpio.out_type = HAL_GPIO_OUT_TYPE_PP;
    
    hal_gpio_init_output(GPIOG, gpio);
    
    osThreadCreate(osThread(thread1), NULL);
    osThreadCreate(osThread(thread2), NULL);
}


/* -- Local function definitions
 * ------------------------------------------------------------------------- */

/**
 *  \brief  Wait loop.
 *  \param  value : Iterations to wait. Blocks execution of other threads.
 */
static void wait_blocking(uint32_t value)
{
    for (; value > 0u; value--);
}
