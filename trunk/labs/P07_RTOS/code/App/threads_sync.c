/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zurich University of                       -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ------------------------------------------------------------------------- */
/**
 *  \brief  Implementation of module threads_sync.
 * 
 *  $Id: threads_sync.c 1138 2016-12-12 09:27:19Z feur $
 * ------------------------------------------------------------------------- */


/* Standard includes */ 
#include <cmsis_os.h>
#include <hal_gpio.h>
#include <stdio.h>

/* User includes */
#include "threads.h"
#include "uart.h"


/* -- Macros
 * ------------------------------------------------------------------------- */

#define LED_GREEN           (0x1 << 13u)
#define LED_RED             (0x1 << 14u)
#define SEMAPHORE_ENABLE    (1)


/* -- Variables with global scope
 * ------------------------------------------------------------------------- */

/* -- Local function declarations
 * ------------------------------------------------------------------------- */

/* -- Thread / semaphore definition
 * ------------------------------------------------------------------------- */

#if SEMAPHORE_ENABLE == 1
osSemaphoreId semaphore_green;
osSemaphoreDef(semaphore_green);
osSemaphoreId semaphore_red;
osSemaphoreDef(semaphore_red);
#endif

void thread1(void const *arg) {
    while (1) {
#if SEMAPHORE_ENABLE == 1
        osSemaphoreWait(semaphore_green, osWaitForever);
#endif
        hal_gpio_bit_set(GPIOG, LED_GREEN);
        osDelay(1000);
        hal_gpio_bit_reset(GPIOG, LED_GREEN);
#if SEMAPHORE_ENABLE == 1
        osSemaphoreRelease(semaphore_red);
#endif
    }
}

osThreadDef(thread1, osPriorityNormal, 1, 0);

void thread2(void const *arg) {
    while (1) {
#if SEMAPHORE_ENABLE == 1
        osSemaphoreWait(semaphore_red, osWaitForever);
#endif
        hal_gpio_bit_set(GPIOG, LED_RED);
        osDelay(1000);
        hal_gpio_bit_reset(GPIOG, LED_RED);
#if SEMAPHORE_ENABLE == 1
        osSemaphoreRelease(semaphore_green);
#endif
    }
}

osThreadDef(thread2, osPriorityNormal, 1, 0);


/* -- Public function definitions
 * ------------------------------------------------------------------------- */

/*
 * See header file
 */
void threads_init(void)
{
    hal_gpio_output_t gpio;
    
    GPIOG_ENABLE();
  
    uart_init();
    
    gpio.pins = LED_GREEN | LED_RED;
    gpio.pupd = HAL_GPIO_PUPD_UP;
    gpio.out_speed = HAL_GPIO_OUT_SPEED_100MHZ;
    gpio.out_type = HAL_GPIO_OUT_TYPE_PP;
    
    hal_gpio_init_output(GPIOG, gpio);
    
#if SEMAPHORE_ENABLE == 1
    semaphore_red = osSemaphoreCreate(osSemaphore(semaphore_red), 1);
    semaphore_green = osSemaphoreCreate(osSemaphore(semaphore_green), 0);
#endif
    
    osThreadCreate(osThread(thread1), NULL);
    osThreadCreate(osThread(thread2), NULL);
}


/* -- Local function definitions
 * ------------------------------------------------------------------------- */

/// STUDENTS: To be programmed




/// END: To be programmed

