/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zurich University of                       -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ------------------------------------------------------------------------- */
/**
 *  \brief  Implementation of module threads_uart.
 * 
 *  $Id: threads_uart.c 1138 2016-12-12 09:27:19Z feur $
 * ------------------------------------------------------------------------- */


/* Standard includes */ 
#include <cmsis_os.h>
#include <stdio.h>

/* User includes */
#include "threads.h"
#include "uart.h"

/* -- Macros
 * ------------------------------------------------------------------------- */

#define MUTEX_ENABLE        (1)
#define HALF_SECOND         (0x7fffff)


/* -- Variables with global scope
 * ------------------------------------------------------------------------- */


/* -- Variables with module wide scope
 * ------------------------------------------------------------------------- */

static uint32_t count = 0u;


/* -- Local function declarations
 * ------------------------------------------------------------------------- */

static void wait_blocking(uint32_t value);


/* -- Thread / mutex definition
 * ------------------------------------------------------------------------- */
 
#if MUTEX_ENABLE
osMutexId uart_mutex;
osMutexDef(uart_mutex);
#endif

void thread1(void const *arg) {
    while (1) {
#if MUTEX_ENABLE
        osMutexWait(uart_mutex, osWaitForever);
#endif
        printf("thread1: count %d\r\n", ++count);
        wait_blocking(HALF_SECOND);
#if MUTEX_ENABLE
        osMutexRelease(uart_mutex);
#endif
    }
}

osThreadDef(thread1, osPriorityNormal, 1, 0);

void thread2(void const *arg) {
    while (1) {
#if MUTEX_ENABLE
        osMutexWait(uart_mutex, osWaitForever);
#endif
        printf("thread2: count %d\r\n", ++count);
        wait_blocking(HALF_SECOND);
#if MUTEX_ENABLE
        osMutexRelease(uart_mutex);
#endif
    }
}

osThreadDef(thread2, osPriorityNormal, 1, 0);


/* -- Public function definitions
 * ------------------------------------------------------------------------- */

/*
 * See header file
 */
void threads_init(void)
{
    uart_init();
    
#if MUTEX_ENABLE
    uart_mutex = osMutexCreate(osMutex(uart_mutex));
#endif
    
    osThreadCreate(osThread(thread1), NULL);
    osThreadCreate(osThread(thread2), NULL);
}


/* -- Local function definitions
 * ------------------------------------------------------------------------- */


/**
 *  \brief  Wait loop.
 *  \param  value : Iterations to wait. Blocks execution of other threads.
 */
static void wait_blocking(uint32_t value)
{
    for (; value > 0u; value--);
}

