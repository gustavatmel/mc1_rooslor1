/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zurich University of                       -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ------------------------------------------------------------------------- */
/**
 *  \brief  Implementation of module threads_button.
 * 
 *  $Id: threads_button.c 1138 2016-12-12 09:27:19Z feur $
 * ------------------------------------------------------------------------- */


/* Standard includes */ 
#include <cmsis_os.h>
#include <hal_gpio.h>
#include <stdio.h>

/* User includes */
#include "threads.h"
#include "uart.h"


/* -- Macros
 * ------------------------------------------------------------------------- */

#define LED_GREEN           (0x1 << 13u)
#define USER_BUTTON         (0x1 << 0u)


/* -- Variables with global scope
 * ------------------------------------------------------------------------- */
 osThreadId tid2;


/* -- Local function declarations
 * ------------------------------------------------------------------------- */


/* -- Thread definition
 * ------------------------------------------------------------------------- */

void thread1(void const *arg) {
    while (1) {
        if (hal_gpio_input_read(GPIOA) & USER_BUTTON) {
            osSignalSet(tid2, 0x1);
        }
    }
}

osThreadDef(thread1, osPriorityNormal, 1, 0);

void thread2(void const *arg) {
    while (1) {
        osSignalWait(0x1, osWaitForever);
        hal_gpio_bit_toggle(GPIOG, LED_GREEN);
    }
}

osThreadDef(thread2, osPriorityNormal, 1, 0);


/* -- Public function definitions
 * ------------------------------------------------------------------------- */

/*
 * See header file
 */
void threads_init(void)
{
    hal_gpio_input_t gpio_in;
    hal_gpio_output_t gpio_out;
    
    GPIOA_ENABLE();
    GPIOG_ENABLE();
  
    uart_init();
    
    gpio_in.pins = USER_BUTTON;
    gpio_in.pupd = HAL_GPIO_PUPD_NOPULL;
    
    hal_gpio_init_input(GPIOA, gpio_in);
    
    gpio_out.pins = LED_GREEN;
    gpio_out.pupd = HAL_GPIO_PUPD_UP;
    gpio_out.out_speed = HAL_GPIO_OUT_SPEED_100MHZ;
    gpio_out.out_type = HAL_GPIO_OUT_TYPE_PP;
    
    hal_gpio_init_output(GPIOG, gpio_out);
    
    tid2 = osThreadCreate(osThread(thread2), NULL);
    osThreadCreate(osThread(thread1), NULL);
}


/* -- Local function definitions
 * ------------------------------------------------------------------------- */

/// STUDENTS: To be programmed




/// END: To be programmed

