/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zurich University of                       -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ------------------------------------------------------------------------- */
/** 
 *  \brief  Implementation of module stop_watch_ctrl.
 * 
 *  $Id: stop_watch_ctrl.c 1064 2016-11-18 06:12:26Z feur $
 * ------------------------------------------------------------------------- */
 

/* User includes */
#include "stop_watch_ctrl.h"
#include "stop_watch.h"
#include "queue.h"
#include "lcd.h"

/* -- Macros
 * ------------------------------------------------------------------------- */

//                           "                    "
#define TEXT_LINE_1         ("STOP WATCH    0>MODE")
#define TEXT_LINE_2_START   ("              1>GO  ")
#define TEXT_LINE_2_RESET   ("              1>RST ")
#define TEXT_LINE_2_STOP    ("              1>STOP")

/// STUDENTS: To be programmed

/* -- Variables with module-wide scope
 * ------------------------------------------------------------------------- */
typedef enum {
    RUN,
    STOP,
		RESET
} sw_ctrl_state_t;

/* event queue for this FSM */
static queue_t sw_ctrl_queue;
static sw_ctrl_state_t state = RUN;
static seg7_output_t output;

void sw_ctrl_put_queue(stop_watch_ctrl_events_t event) {
	queue_enqueue(&sw_ctrl_queue, event);
}

void sw_ctrl_handle_event(void) {
		uint32_t event;
    
    event = queue_dequeue(&sw_ctrl_queue);
   
    switch (event) {
				case SWC_DISPLAY_UPDATE_EVENT:
            sw_ctrl_update_display();
            break;
				case SWC_BUTTON_EVENT:
						switch (state) {
								case (RUN):
										state = STOP;
										stop_watch_start();
										break;
								case (STOP):
										state = RESET;
										stop_watch_stop();
										break;
								case (RESET):
										state = RUN;
										stop_watch_reset();
										break;
						}
						break;
    }
    
    seg7_output_update();
    lcd_output_update();
}

void sw_ctrl_update_display(void) {
		lcd_write(LCD_LINE_1, TEXT_LINE_1);
    
    switch (state) {
        case RUN:
            lcd_write(LCD_LINE_2, TEXT_LINE_2_START);
            break;
        case STOP:
            lcd_write(LCD_LINE_2, TEXT_LINE_2_STOP);
            break;
				case RESET:
            lcd_write(LCD_LINE_2, TEXT_LINE_2_RESET);
            break;
    }
}




/// END: To be programmed
