/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zurich University of                       -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ------------------------------------------------------------------------- */
/** 
 *  \brief  Implementation of module stop_watch.
 * 
 *  $Id: stop_watch.c 1064 2016-11-18 06:12:26Z feur $
 * ------------------------------------------------------------------------- */
 

/* User includes */
#include "stop_watch.h"
#include "scheduler.h"
#include "time.h"

/// STUDENTS: To be programmed
static scheduler_task_t task_increment;
static time_t sw_timer = { 0u, 0u};


/* -- Local function declarations
 * ------------------------------------------------------------------------- */
static void sw_timer_increment(void);



void stop_watch_start(void) {
  seg7_output_update();
  scheduler_register_task(&task_increment, sw_timer_increment, HUNDRED_MILLISECONDS);
}

void stop_watch_stop(void) {
		scheduler_unregister_task(&task_increment);
}

void stop_watch_reset(void) {
		sw_timer.seconds = 0u;
		sw_timer.minutes = 0u;
}

void stop_watch_get_output(seg7_output_t *output) {
		output->value = time_get_bcd(&sw_timer);
		output->dots = (0x1 << 1u);  
}

/* -- Local function definitions
 * ------------------------------------------------------------------------- */

/**
 *  \brief  Increment timer variable.
 */
static void sw_timer_increment(void)
{
    time_seconds_inc(&sw_timer);

    // output data
    seg7_output_update();
}



/// END: To be programmed
