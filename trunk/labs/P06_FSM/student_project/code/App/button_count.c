/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zurich University of                       -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ------------------------------------------------------------------------- */
/** 
 *  \brief  Implementation of module button_count.
 * 
 *  $Id: button_count.c 1064 2016-11-18 06:12:26Z feur $
 * ------------------------------------------------------------------------- */
 

/* User includes */
#include "button_count.h"
#include "queue.h"
#include "lcd.h"
#include "reg_ctboard.h"

/* -- Macros
 * ------------------------------------------------------------------------- */

//                           "                    "
#define TEXT_LINE_1         ("BUTTONS       0>MODE")
#define TEXT_LINE_2_T0      ("              1>T0  ")
#define TEXT_LINE_2_T1      ("              1>T1  ")
#define TEXT_LINE_2_T2      ("              1>T2  ")
#define TEXT_LINE_2_T3      ("              1>T3  ")

/// STUDENTS: To be programmed
typedef enum {
    SHOWT0,
    SHOWT1,
		SHOWT2,
		SHOWT3
} bcc_ctrl_state_t;

/* -- Local function declarations
 * ------------------------------------------------------------------------- */

static void button_count_observer(uint32_t data);

/* event queue for this FSM */
static queue_t bcc_ctrl_queue;
static bcc_ctrl_state_t state = SHOWT0;
static seg7_output_t output;
static uint16_t counter[4] = {0,0, 0, 0};

void button_count_init(void) {
	buttons_register_observer(button_count_observer);
}

static void button_count_observer(uint32_t data) {
	switch (data) {
		case (T0_PRESSED):
				counter[0]++;
				break;
		case (T1_PRESSED):
				counter[1]++;
				break;
		case (T2_PRESSED):
				counter[2]++;
				break;
		case (T3_PRESSED):
				counter[3]++;
				break;
	}
}

void button_count_put_queue(button_count_events_t event) {
	queue_enqueue(&bcc_ctrl_queue, event);
}


void button_count_handle_event(void) {
	uint32_t event;
    
    event = queue_dequeue(&bcc_ctrl_queue);
   
    switch (event) {
				case BCC_DISPLAY_UPDATE_EVENT:
            button_count_update_display();
            break;
				case BCC_BUTTON_T0_EVENT:
						switch (state) {
								case (SHOWT0):
										state = SHOWT1;
										
										break;
								case (SHOWT1):
										state = SHOWT2;
										
										break;
								case (SHOWT2):
										state = SHOWT3;
										
										break;
								case (SHOWT3):
										state = SHOWT0;
										
										break;
						}
						break;
    }
    
    seg7_output_update();
    lcd_output_update();
}


void button_count_get_output(seg7_output_t *output) {
	switch (state) {
		case (SHOWT0):
			output->value = counter[0]; 
			break;
		case (SHOWT1):
			output->value = counter[1]; 
			break;
		case (SHOWT2):
			output->value = counter[2];  
			break;
		case (SHOWT3):
			output->value = counter[3]; 
			break;
	}
	 
}


void button_count_update_display(void) {
	lcd_write(LCD_LINE_1, TEXT_LINE_1);
    
    switch (state) {
        case SHOWT0:
            lcd_write(LCD_LINE_2, TEXT_LINE_2_T1);
            break;
        case SHOWT1:
            lcd_write(LCD_LINE_2, TEXT_LINE_2_T2);
            break;
				case SHOWT2:
            lcd_write(LCD_LINE_2, TEXT_LINE_2_T3);
            break;
				case SHOWT3:
            lcd_write(LCD_LINE_2, TEXT_LINE_2_T0);
            break;
    }
}



/// END: To be programmed
