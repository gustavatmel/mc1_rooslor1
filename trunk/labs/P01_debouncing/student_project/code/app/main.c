/* -----------------------------------------------------------------
 * --  _____       ______  _____                                    -
 * -- |_   _|     |  ____|/ ____|                                   -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems    -
 * --   | | | '_ \|  __|  \___ \   Zurich University of             -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                 -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland     -
 * ------------------------------------------------------------------
 * --
 * -- main.c
 * --
 * -- main for MC1 lab on debouncing
 * --
 * -- $Id: main.c 1203 2017-09-14 09:00:23Z kjaz $
 * ------------------------------------------------------------------
 */
#include "hal_gpio.h"
#include "hal_rcc.h"
#include "reg_ctboard.h"

#define BITMASK_KEY_0       0x01
#define BITMASK_KEY_1       0x02
#define BITMASK_KEY_2       0x04
#define BITMASK_KEY_3       0x08
#define BITMASK_KEY_ALL     0x0F

#define NR_OF_7SEG_DISPLAYS 4u
#define NR_SAMPLES          50  // set to 50 for stable generic debounce

#define GPIO_P6             (uint16_t)0x0040
#define GPIO_P3_to_P0       (uint16_t)0x000F

/*  typedef used for Zusatzaufgabe */
typedef struct {
    uint16_t samples[NR_SAMPLES];   // space to store the samples
    uint8_t write_index;            // index where next sample will be written
                                    // to array
} sample_buffer_t;

/* forward declarations of internal functions */
static uint8_t detect_switch_change_debounce(void);
static uint8_t detect_switch_change(void);
static uint8_t generic_debounce(sample_buffer_t *sample_buffer,
                                uint16_t new_sample);
static void output_7seg_digits(uint8_t values[]);

int main(void)
{
    uint8_t switch_change = 0;
    uint8_t count[NR_OF_7SEG_DISPLAYS];
    uint32_t i;

    /* additional variable definitions go here */
    /// STUDENTS: To be programmed

		hal_gpio_input_t portDefInput = {GPIO_P3_to_P0, HAL_GPIO_PUPD_UP};
		hal_gpio_output_t portDefOutput = {GPIO_P6, HAL_GPIO_PUPD_NOPULL, HAL_GPIO_OUT_SPEED_2MHZ, HAL_GPIO_OUT_TYPE_PP};
		
		sample_buffer_t newBuffer;
			
    /// END: To be programmed

    /* initialize counters */
    for (i = 0; i < NR_OF_7SEG_DISPLAYS; i++) {
        count[i] = 0u;
    }

    /* enable clock on GPIOA and GPIOB */
    GPIOA_ENABLE();
    GPIOB_ENABLE();

    GPIOA_RESET();
    GPIOB_RESET();

    /* configure GPIO pins --> task 4.2 */
    /// STUDENTS: To be programmed


		hal_gpio_init_input(GPIOB, portDefInput);
		hal_gpio_init_output(GPIOB, portDefOutput);
		

    /// END: To be programmed

    while (1) {
        switch (CT_DIPSW->BYTE.S7_0 & 0x03) {
            case    0x00:
                /* Task 4.2: Edge detection NO debounce */
                switch_change = detect_switch_change();
                break;
            case    0x01:
                /* Task 4.3: Edge detection WITH debounce */
                switch_change = detect_switch_change_debounce();
                break;
            default:
                /* Zusatzaufgabe */
                /// STUDENTS: To be programmed

						
						    switch_change = generic_debounce(&newBuffer, hal_gpio_input_read(GPIOB));


                /// END: To be programmed
                break;
        }

        /* increment the appropriate counter if a key has been switched */
        if (switch_change < NR_OF_7SEG_DISPLAYS) {
            count[switch_change]++;
        }

        output_7seg_digits(count);
    }
}

/**
 *  \brief  Evaluates dip switch changes on PB[3:0] since last function call.
 *
 *  Evaluates whether one of the dip switches has changed since the last call
 *  to this function. The function takes a sample of ports PB[3:0] and compares
 *  it to the sample values taken during the previous function call. In cases
 *  where more than one dip_switch has changed, only the changed dip_switch
 *  with the lowest index is returned. The others are ignored.
 *
 *  \param  none
 *  \return 00  --> change on PB0
 *          01  --> change on PB1
 *          02  --> change on PB2
 *          03  --> change on PB3
 *          FF  --> no change
 *
 */
uint8_t detect_switch_change(void)
{
    /// STUDENTS: To be programmed
	
		//hal_gpio_bit_toggle(GPIOB, GPIO_P6);

		static uint16_t previous_switch_values = 0;
		uint16_t now_switch_values;
		uint16_t switch_change_func;
	
		now_switch_values = hal_gpio_input_read(GPIOB);
	
		
		switch_change_func = (now_switch_values ^ previous_switch_values) & BITMASK_KEY_ALL;
	
	
		previous_switch_values = now_switch_values;
	
		if (switch_change_func == BITMASK_KEY_0) {
				return 0x00;
		}
		else if (switch_change_func == BITMASK_KEY_1) {
				return 0x01;
		}
		else if (switch_change_func == BITMASK_KEY_2) {
				return 0x02;
		}
		else if (switch_change_func == BITMASK_KEY_3) {
				return 0x03;
		}
		else {
				return 0xFF;
		}
		

    /// END: To be programmed
}

/**
 *  \brief  Evaluates dip switch changes on PB[3:0] with debouncing.
 *
 *  The function keeps a window of the last NR_SAMPLES on port PB[3:0]. On each
 *  call a new sample is taken and the oldest sample is discarded. The window
 *  is then searched for rising and falling edges. The lowest index with an
 *  edge is returned. Other edges are ignored.
 *
 *  \param  none
 *  \return 00  --> change on PB0
 *          01  --> change on PB1
 *          02  --> change on PB2
 *          03  --> change on PB3
 *          FF  --> no change
 *
 */
static uint8_t detect_switch_change_debounce(void)
{
    /// STUDENTS: To be programmed
	static uint16_t switchSamples[NR_SAMPLES];
	uint8_t stable = 1; //1 = true, 0 = false
	
	//shift old values to highest array bit and check if stable
	for (int i = NR_SAMPLES; i > 0; i--) {
		if (switchSamples[i] ^ switchSamples[i-1]) {
			stable = 0;
		}
		switchSamples[i] = switchSamples[i-1];
	}
	//read new value
	switchSamples[0] = hal_gpio_input_read(GPIOB);
	
	//only look for change if stable
	if (stable == 1) {
		uint8_t change = (switchSamples[0] ^ switchSamples[1]) & BITMASK_KEY_ALL;
		
		//dedect any chnage in newest value			
				if (change == BITMASK_KEY_0) {
					return 0x00;
				}
				else if (change == BITMASK_KEY_1) {
					return 0x01;
				}
				else if (change == BITMASK_KEY_2) {
					return 0x02;
				}
				else if (change == BITMASK_KEY_3) {
					return 0x03;
				}
				else {
					return 0xFF;
				}
	}
		
    /// END: To be programmed
}


/**
 *  \brief  Maintain sample history and look for edges with debouncing applied.
 *
 *  On each call the oldest sample is discarded and new_sample is added to the
 *  history. The history window is then searched for rising and falling edges.
 *  The lowest index with an edge is returned. Other edges are ignored.
 *
 *  \param  sample_buffer   pointer to a struct allocated by caller
 *                          The struct is organized as a ring buffer. It 
 *                          contains the sample history and an index where a
 *                          new sample shall be inserted.
 *                           
 *  \param  new_sample      The current 4-bit sample as read from the GPIO pins
 *
 *  \return 00  --> edge on bit0
 *          01  --> edge on bit1
 *          02  --> edge on bit2
 *          03  --> edge on bit3
 *          FF  --> no edge
 *
 */
static uint8_t generic_debounce(sample_buffer_t *sample_buffer,
                                uint16_t new_sample)
{
    /// STUDENTS: To be programmed

	
	uint8_t stable = 1; //1 = true, 0 = false
	uint8_t ret = 0xFF;
	
	//shift old values to highest array bit and check if stable
	for (int i = NR_SAMPLES-1; i > 0; i--) {
		if (sample_buffer->samples[i] ^ sample_buffer->samples[i-1] && stable == 1) {
			stable = 0;
		}
		sample_buffer->samples[i] = sample_buffer->samples[i-1];
	}
	//read new value
	sample_buffer->samples[0] = (new_sample & BITMASK_KEY_ALL);
	
	//only look for change if stable
	if (stable == 1) {
		uint8_t change = (sample_buffer->samples[0] ^ sample_buffer->samples[1]);
		
		//dedect any chnage in newest value			
				if (change == BITMASK_KEY_0) {
					ret = 0x00;
				}
				else if (change == BITMASK_KEY_1) {
					ret = 0x01;
				}
				else if (change == BITMASK_KEY_2) {
					ret = 0x02;
				}
				else if (change == BITMASK_KEY_3) {
					ret = 0x03;
				}
				
				
	}
	
	return ret;


    /// END: To be programmed
}


/**
 *  \brief  Write lower nibbles of the 4 values to the 7-seg displays
 *
 *  \param  values[]    An array of four 8-bit values to be displayed on 7-seg
 */
static void output_7seg_digits(uint8_t values[])
{
    /* array allows simple conversion from int to seven-segment code */
    const uint8_t seven_seg_patterns[16] = { 0xc0, 0xf9, 0xa4, 0xb0,
                                             0x99, 0x92, 0x82, 0xf8,
                                             0x80, 0x90, 0x88, 0x83,
                                             0xc6, 0xa1, 0x86, 0x8e };

    /* Write to 7-segment display */
    CT_SEG7->RAW.BYTE.DS0 = seven_seg_patterns[values[0] & 0x0F];
    CT_SEG7->RAW.BYTE.DS1 = seven_seg_patterns[values[1] & 0x0F];
    CT_SEG7->RAW.BYTE.DS2 = seven_seg_patterns[values[2] & 0x0F];
    CT_SEG7->RAW.BYTE.DS3 = seven_seg_patterns[values[3] & 0x0F];
}
