/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zurich University of                       -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ------------------------------------------------------------------------- */
/**
 *  \brief  Implementation of module pwm.
 *
 *  \file   pwm.c
 *  $Id: pwm.c 1246 2017-10-18 12:44:28Z kjaz $
 * ------------------------------------------------------------------------- */


/* Standard includes */
#include <hal_rcc.h>
#include <hal_gpio.h>
#include <hal_timer.h>
#include <hal_adc.h>
#include <hal_dma.h>

/* User includes */
#include "pwm.h"


/* -- Macros
 * ------------------------------------------------------------------------- */

#define PWM_CHANNEL_RED   HAL_TIMER_CH1
#define PWM_CHANNEL_GREEN HAL_TIMER_CH2
#define PWM_CHANNEL_BLUE  HAL_TIMER_CH3


/* -- Local function declarations
 * ------------------------------------------------------------------------- */

static void pwm_init_timer(void);
static void pwm_init_adc(void);
static void pwm_init_dma(void);


/* -- Public function definitions
 * ------------------------------------------------------------------------- */

/*
 *  See header file
 */
void pwm_init(void)
{
    pwm_init_timer();
    pwm_init_adc();
    pwm_init_dma();

    hal_timer_start(TIM3);
    hal_adc_start(ADC3);
    /// STUDENTS: To be programmed




    /// END: To be programmed
}


/* -- Local function definitions
 * ------------------------------------------------------------------------- */

/**
 *  \brief  Initializes timer for pwm signal
 */
static void pwm_init_timer(void)
{
    hal_gpio_output_t gpio_init;
    hal_timer_base_init_t timer_init;
    hal_timer_output_init_t output_init;

    /* Configure GPIO output */
    gpio_init.pins = 0x31;          // Pin 0,4 and 5 as PWM output
    gpio_init.out_speed = HAL_GPIO_OUT_SPEED_10MHZ;
    gpio_init.out_type = HAL_GPIO_OUT_TYPE_PP;
    gpio_init.pupd = HAL_GPIO_PUPD_DOWN;

    hal_gpio_init_alternate(GPIOB, HAL_GPIO_AF_TIM3, gpio_init);

    /* Basic timer configuration */
    timer_init.mode = HAL_TIMER_MODE_UP;
    timer_init.run_mode = HAL_TIMER_RUN_CONTINOUS;
    timer_init.prescaler = 8192;
    timer_init.count = 63;

    hal_timer_init_base(TIM3, timer_init);

    /* Configure PWM channels */
    output_init.mode = HAL_TIMER_OCMODE_PWM1;
    output_init.pulse = 0u;             // Initial PWM value (compare reg)
    output_init.output_state = ENABLED;
    output_init.polarity = HAL_TIMER_POLARITY_HIGH;

    hal_timer_init_output(TIM3, PWM_CHANNEL_RED, output_init);
    hal_timer_init_output(TIM3, PWM_CHANNEL_GREEN, output_init);
    hal_timer_init_output(TIM3, PWM_CHANNEL_BLUE, output_init);
}


/**
 *  \brief  Initializes ADC for scanning potentiometer
 */
static void pwm_init_adc(void)
{
    hal_gpio_input_t gpio_init;
    hal_adc_init_t adc_init;
    hal_adc_ch_init_t channel_init;

    /* Configure GPIO analog input */
    gpio_init.pins = (0x1 << 6u);
    gpio_init.pupd = HAL_GPIO_PUPD_NOPULL;

    hal_gpio_init_analog(GPIOF, gpio_init);

    /* Configure ADC3 */
    adc_init.resolution = HAL_ADC_RES_6B;
    adc_init.scan_mode = DISABLE;
    adc_init.continuous_mode = ENABLE;
    adc_init.polarity = HAL_ADC_POLARITY_NONE;  // Triggerdetection
    adc_init.alignment = HAL_ADC_ALIGN_RIGHT;
    adc_init.nr_conversions = 1u;

    hal_adc_init_base(ADC3, adc_init);

    /* Configure ADC3 channel */
    channel_init.rank = 1u;
    channel_init.cycles = HAL_ADC_SAMPLING_144C;

    hal_adc_init_channel(ADC3, HAL_ADC_CH4, channel_init);

    /* Init DMA */
    hal_adc_set_dma(ADC3, ENABLE);
}


/**
 *  \brief  Initializes DMA for transfering scanned data from ADC
 *          to the capture compare register of the timer
 */
static void pwm_init_dma(void)
{
    /// STUDENTS: To be programmed




    /// END: To be programmed
}
