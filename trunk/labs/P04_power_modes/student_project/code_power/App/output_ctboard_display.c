/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zurich University of                       -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ------------------------------------------------------------------------- */
/**
 *  \brief  Implementation of module output (target: CT Board).
 *
 *  $Id: output_ctboard_display.c 997 2016-10-28 09:48:56Z feur $
 * ------------------------------------------------------------------------- */


/* Standard includes */
#include <reg_ctboard.h>

/* User includes */
#include "output.h"
#include "pwm.h"


/* -- Macros
 * ------------------------------------------------------------------------- */

/// STUDENTS: To be programmed




/// END: To be programmed


/* -- Public function definitions
 * ------------------------------------------------------------------------- */

/*
 *  See header file
 */
void output_init(void)
{
    /* Initialize pwm for optional task. */
    /// STUDENTS: To be programmed




    /// END: To be programmed
}


/*
 *  See header file
 */
void output_set_green(hal_bool_t status)
{
    /// STUDENTS: To be programmed




    /// END: To be programmed
}


/*
 *  See header file
 */
void output_set_red(hal_bool_t status)
{
    /// STUDENTS: To be programmed




    /// END: To be programmed
}

