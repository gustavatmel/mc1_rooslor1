/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zurich University of                       -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ------------------------------------------------------------------------- */
/**
 *  \brief  Implementation of module output (target: discovery).
 *
 *  $Id: output_discovery.c 349 2015-10-14 09:25:52Z feur $
 * ------------------------------------------------------------------------- */


/* Standard includes */
#include <reg_stm32f4xx.h>
#include <hal_gpio.h>

/* User includes */
#include "output.h"


/* -- Macros
 * ------------------------------------------------------------------------- */

/// STUDENTS: To be programmed




/// END: To be programmed


/* -- Public function definitions
 * ------------------------------------------------------------------------- */

/*
 *  See header file
 */
void output_init(void)
{
    hal_gpio_output_t gpio_init;


    /// STUDENTS: To be programmed




    /// END: To be programmed
}


/*
 *  See header file
 */
void output_set_green(hal_bool_t status)
{
    /// STUDENTS: To be programmed




    /// END: To be programmed
}


/*
 *  See header file
 */
void output_set_red(hal_bool_t status)
{
    /// STUDENTS: To be programmed




    /// END: To be programmed
}

