/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zurich University of                       -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ------------------------------------------------------------------------- */
/**
 *  \brief  Interface of module pwm.
 *
 *  \file   pwm.h
 *  $Id: pwm.h 1246 2017-10-18 12:44:28Z kjaz $
 * ------------------------------------------------------------------------- */

/* Re-definition guard */
#ifndef _LED_H
#define _LED_H


/* -- Public function declarations
 * ------------------------------------------------------------------------- */

/**
 *  \brief  Initializes the pwm module
 */
void pwm_init(void);
#endif
