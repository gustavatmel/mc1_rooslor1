/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zurich University of                       -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 * --
 * -- Project     : MC1 Lab05 Encoder
 * -- Description : This code controls the module encoder gray
 * --
 * --
 * -- $Id: encoder_gray.h 1054 2016-11-14 18:31:09Z mahi $
 * ------------------------------------------------------------------------- */

/* Re-definition guard */
#ifndef _ENCODER_GRAY_H
#define _ENCODER_GRAY_H

/* User includes */
#include "stdint.h"

/* Public defines  -----------------------------------------------------------*/

/// STUDENTS: To be programmed




/// END: To be programmed

/* Public function definitions -----------------------------------------------*/

/**
 *  \brief  Initialize and activate sensors
 *  \param  -
 */
void encoder_gray_init(void);

/// STUDENTS: To be programmed

void sensor_polling();

/**
 *  \brief  Returns a number out of a gray code (4 bits)
 *  \param  -
 */
int gray_to_number(uint8_t g_bit1, uint8_t g_bit2, uint8_t g_bit3, uint8_t g_bit4);


void write_gray_value();


/// END: To be programmed
#endif
