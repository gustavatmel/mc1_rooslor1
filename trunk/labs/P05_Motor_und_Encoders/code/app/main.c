/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zurich University of                       -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 * --
 * -- Project     : MC1 Lab05 Encoder
 * -- Description : This code controls the dc-motor and the encoders
 * --
 * --				The value of the encoded position will be displayed
 * --				With a button the Forwared-Reverse direction can be set
 * --				For more information see description of the lab.
 * --
 * -- $Id: main.c 1298 2017-11-09 14:47:50Z kjaz $
 * ------------------------------------------------------------------------- */
#include <stdint.h>
#include <stdio.h>

/* User includes */
#include "reg_ctboard.h"
#include "hal_gpio.h"
#include "motor_control.h"
#include "encoder_quad.h"
#include "encoder_gray.h"
#include "hal_ct_lcd.h"
#include "sevenseg.h"


/* User define    -----------------------------------------------------------*/
#define MASK_DIPSW_S31 0x80
#define LCD_ADDR_LINE1 0u
#define LCD_ADDR_LINE2 20u
#define LCD_CLEAR      "                    "
#define LCD_ON         0xffff
#define LCD_OFF        0x0000

#define LED_D2         0x010
#define LED_D1         0x020

/* Type definitions ---------------------------------------------------------*/
typedef enum {
    ENCODER_QUAD = 0x0,
    ENCODER_GRAY = 0x1
} state_encoder_type;

/* Local function declarations --------------------------------------------- */
static void init_components(state_encoder_type state);

/* MAIN FUNCTION ----------------------------------------------------------- */
int main(void)
{
    //----------------------------------------------------
    // Initialization of the variables
    //----------------------------------------------------

    state_encoder_type state;

    /// STUDENTS: To be programmed
	
	uint32_t interrupt_counter = 0;
	uint32_t quadrant_counter = 0;
	int32_t drehzahl = 0;

	set_counters(&interrupt_counter, &quadrant_counter, &drehzahl);


    /// END: To be programmed

    // Switch between quadrature or gray encoder
    // If (S31 == 0) => ENCODER_QUAD selected
    // If (S31 == 1) => ENCODER_GRAY selected
    if (CT_DIPSW->BYTE.S31_24 & MASK_DIPSW_S31) {
        state = ENCODER_GRAY;
    } else {
        state = ENCODER_QUAD;
    }

    // Initializes all components
    init_components(state);

    //----------------------------------------------------
    // Endless loop :
    //----------------------------------------------------
    while (1) {
        if (state == ENCODER_QUAD) {
            /// STUDENTS: To be programmed
					
			int32_t value = abs(drehzahl) / 32;
		
			char letter = '0' + (value / 1000);
			hal_ct_lcd_write(1, &letter);
			
			value %= 1000;
			letter = '0' + (value / 100);
			hal_ct_lcd_write(2, &letter);
			
			value %= 100;
			letter = '0' + (value / 10);
			hal_ct_lcd_write(3, &letter);
			
			value %= 10;
			letter = '0' + value;
			hal_ct_lcd_write(4, &letter);

			show_quadrant_7seg_bars(((quadrant_counter % 32) / 8) + 1);
			show_enc_location_7seg(((quadrant_counter % 32) / 2) + 1);

            /// END: To be programmed
        } else {
            /// STUDENTS: To be programmed

						sensor_polling();
					
						write_gray_value();


            /// END: To be programmed
        }
        // Function processes potentiometer to control the motor
        motor_control_excecute();
    }
}



/**
 *  \brief  Initializes components: Encoder | Motor | Poti | Display
 *  \param  state: define used encoder to make encoder specific initialisation
 */
static void init_components(state_encoder_type state)
{
    // Disable all interrupts
    RCC->CIR = 0x00000000;

    // Enable syscfg clock
    SYSCFG_ENABLE();

    // Initialize display and apply background color
    hal_ct_lcd_color(HAL_LCD_GREEN, LCD_ON);  // green on
    hal_ct_lcd_color(HAL_LCD_RED, LCD_OFF);   // red off
    hal_ct_lcd_color(HAL_LCD_BLUE, LCD_OFF);  // blue off

    // Clear display
    hal_ct_lcd_clear();

    // Initializes the motor (PWM signal, speed, direction)
    motor_init();

    if (state == ENCODER_QUAD) {
        encoder_quad_init();
    } else {
        encoder_gray_init();
    }
}
