/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zurich University of                       -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 * --
 * -- Project     : MC1 Lab05 Encoder
 * -- Description : This code controls the module encoder gray
 * --
 * --
 * -- $Id: encoder_gray.c 1060 2016-11-16 13:56:35Z mahi $
 * ------------------------------------------------------------------------- */

/* Includes ------------------------------------------------------------------*/
#include "reg_ctboard.h"
#include "encoder_gray.h"
#include "sensor.h"

/* User define    -----------------------------------------------------------*/

/* Variables and functions with module wide scope ----------------------------*/

/// STUDENTS: To be programmed

		static uint8_t sensor_u3;
		static uint8_t sensor_u4;
		static uint8_t sensor_u5;
		static uint8_t sensor_u6;


/// END: To be programmed

/* Public functions definitions -----------------------------------------------*/

/*
 * See header file
 */
void encoder_gray_init(void)
{
    // Init SENSOR_Ux GPIOs as input
    sensor_init();

    /// STUDENTS: To be programmed

    sensor_init();

    /// STUDENTS: To be programmed

    /// STUDENTS: To be programmed
	
		sensor_enable_all();


    /// END: To be programmed
}

/// STUDENTS: To be programmed

void sensor_polling() {
	
		sensor_u3 = sensor_read_single(SENSOR_U3);
		sensor_u4 = sensor_read_single(SENSOR_U4);
		sensor_u5 = sensor_read_single(SENSOR_U5);
		sensor_u6 = sensor_read_single(SENSOR_U6);
	
}

int gray_to_number(uint8_t g_bit1, uint8_t g_bit2, uint8_t g_bit3, uint8_t g_bit4) {
	uint8_t b_bit1 = g_bit1;
	uint8_t b_bit2 = g_bit2 ^ b_bit1;
	uint8_t b_bit3 = g_bit3 ^ b_bit2;
	uint8_t b_bit4 = g_bit4 ^ b_bit3;

	return (8 * b_bit1) + (4 * b_bit2) + (2 * b_bit3) + (1 * b_bit4);
}

void write_gray_value() {
	uint8_t value = gray_to_number(sensor_u3, sensor_u4, sensor_u5, sensor_u6);
	CT_LED->HWORD.LED15_0 = (uint16_t) 0x1 << value; 

}


/// END: To be programmed


/// END: To be programmed
