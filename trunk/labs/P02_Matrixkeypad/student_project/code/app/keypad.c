/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zurich University of                       -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 * --
 * -- Project     : CT2 Lab Matrix-Keypad
 * -- Description : Modul handles the reading of the keypad
 * --
 * -- $Id: keypad.c 899 2016-09-27 06:31:38Z ruan $
 * ------------------------------------------------------------------------- */

#include "keypad.h"
#include "hal_gpio.h"
/* -- defines
 * --------------------------------------------------------------------------*/
/// STUDENTS: To be programmed
#define GPIO_KEYPAD 0x0FF
#define ROWS_MASK   (uint16_t) 0x0F0;
#define COLS_MASK   (uint16_t) 0x00F;
/// END: To be programmed

/* type definitions -------------------------------------------------------- */
/// STUDENTS: To be programmed

hal_gpio_input_t gpio_in;
hal_gpio_output_t gpio_out;

/// END: To be programmed

/* internal function declarations ------------------------------------------ */

/// STUDENTS: To be programmed

void set_cols_out_rows_in(void);
void set_rows_out_cols_in(void);
void set_cols_low(void);
void set_rows_low(void);
uint8_t is_f_pressed(uint16_t row_value, uint16_t col_value);

/// END: To be programmed

/* internal variable definitions ------------------------------------------- */

const int key_array [4][4] = { { 0x1, 0x2, 0x3, 0xF },
                               { 0x4, 0x5, 0x6, 0xE },
                               { 0x7, 0x8, 0x9, 0xD },
                               { 0xA, 0x0, 0xB, 0xC } };
const uint8_t fast_lookup[9] = {NOKEY_PRESSED, 0, 1, 255, 2, 255, 255, 255, 3};

/* public function definitions --------------------------------------------- */
/*
 * See header file
 */
uint8_t scan_matrix_keypad_cbc(void)
{
    uint8_t ret_val = NOKEY_PRESSED;
    /// STUDENTS: To be programmed
    uint8_t i;
    uint8_t j;
    uint16_t col_values;
    uint16_t row_values;
    
    set_cols_out_rows_in();
    
    for (i = 0; i < 4; i++) {
        
        // Write high to all columns, then set current column to low
        col_values = COLS_MASK;
        hal_gpio_output_write(GPIOB, col_values);
        hal_gpio_bit_reset(GPIOB, 1 << i);
        
        // Read row values and shift to LSB
        row_values = hal_gpio_input_read(GPIOB);
        row_values = ~row_values;   // We need to negate since it's active low
        row_values &= ROWS_MASK;
        row_values >>= 4;
        
        // Shift rows until pressed key is found
        if (row_values > 0) {
            for (j = 0; j < 4; j++) {
                if (row_values & 0x1) {
                    ret_val = key_array[j][i];
                    break;
                }
                row_values >>= 1;
            }
        }
    }    
    
    /// END: To be programmed
    return ret_val;
}

/*
 * See header file
 */
uint8_t scan_matrix_keypad_fast(uint8_t use_shiftkey)
{
    uint8_t ret_val = NOKEY_PRESSED;
    /// STUDENTS: To be programmed
    uint16_t col_values;
    uint16_t row_values;
    uint8_t row_idx;
    uint8_t col_idx;
    uint8_t is_shift = 0;
    
    // Set columns as output write low, read rows
    set_cols_out_rows_in();
    set_cols_low();
    row_values = hal_gpio_input_read(GPIOB);
    row_values = ~row_values;
    row_values &= ROWS_MASK;
    row_values >>= 4;
    
    // Set rows as output, write low, read columns
    set_rows_out_cols_in();
    set_rows_low();
    col_values = hal_gpio_input_read(GPIOB);
    col_values = ~col_values;
    col_values &= COLS_MASK;
    
 
    // Check if f is pressed and mask it
    if (use_shiftkey && (row_values & 0x1) && (col_values & 0x8)) {
        if (row_values > 0x1) {
            row_values &= 0xFE;
        }
        if (col_values > 0x8) {
            col_values &= 0xF7;
        }
        is_shift = 1;
    }
    
    // Look up index in lookup table
    row_idx = fast_lookup[row_values];
    col_idx = fast_lookup[col_values];
    ret_val = key_array[row_idx][col_idx];
    
    // Modify ret_val if shift is not the only pressed key
    if (is_shift && (ret_val != 0x0F)) {
        ret_val |= 0x10;
    }
    
    /// END: To be programmed
    return ret_val;
}

/*
 * See header file
 */
void keypad_init_gpio(void)
{
    hal_gpio_input_t gpio_keypad = {
        GPIO_KEYPAD,
        HAL_GPIO_PUPD_NOPULL // may need to be UP
    };
    hal_gpio_init_input(GPIOB, gpio_keypad);
}
/* internal functions definitions ------------------------------------------ */
/// STUDENTS: To be programmed

void set_cols_out_rows_in() {
    gpio_out.pins = COLS_MASK;
    gpio_out.pupd = HAL_GPIO_PUPD_UP;
    gpio_out.out_speed = HAL_GPIO_OUT_SPEED_100MHZ;
    gpio_out.out_type = HAL_GPIO_OUT_TYPE_PP;
    
    hal_gpio_init_output(GPIOB, gpio_out);
    
    
    gpio_in.pins = ROWS_MASK;
    gpio_in.pupd = HAL_GPIO_PUPD_UP;
    
    hal_gpio_init_input(GPIOB, gpio_in);
}

void set_rows_out_cols_in() {
    gpio_out.pins = ROWS_MASK;
    gpio_out.pupd = HAL_GPIO_PUPD_UP;
    gpio_out.out_speed = HAL_GPIO_OUT_SPEED_100MHZ;
    gpio_out.out_type = HAL_GPIO_OUT_TYPE_PP;
    
    hal_gpio_init_output(GPIOB, gpio_out);
    
    
    gpio_in.pins = COLS_MASK;
    gpio_in.pupd = HAL_GPIO_PUPD_UP;
    
    hal_gpio_init_input(GPIOB, gpio_in);
}

void set_cols_low() {
    uint16_t value;
    value = hal_gpio_input_read(GPIOB);
    value &= ~COLS_MASK;
    hal_gpio_output_write(GPIOB, value);
}

void set_rows_low() {
    uint16_t value;
    value = hal_gpio_input_read(GPIOB);
    value &= ~ROWS_MASK;
    hal_gpio_output_write(GPIOB, value);
}


/// END: To be programmed

