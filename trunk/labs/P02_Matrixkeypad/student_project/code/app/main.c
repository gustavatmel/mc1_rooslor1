/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zurich University of                       -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 * --
 * -- Project     : MC1 Lab Matrix-Keypad
 * -- Description : This code handles the reading of a 4x4
 * --               matrix-keypad (0..F)
 * --				The value of the pressed key will be displayed
 * --				on a 8-digit 7-segment-display.
 * --				For more information see description of the internship.
 * --
 * -- $Id: main.c 898 2016-09-27 06:13:57Z ruan $
 * ------------------------------------------------------------------------- */

/* includes ---------------------------------------------------------------- */
/// STDENTS: To be programmed
#include <stdint.h>
#include "hal_gpio.h"
#include "hal_rcc.h"
#include "keypad.h"
#include "display.h"
#include "inout.h"
/// END: To be programmed

/* -- defines----------------------------------------------------------------*/
/// STUDENTS: To be programmed




/// END: To be programmed


/* -- local function declarations -------------------------------------------*/


/* ----------------------------------------------------------------------------
 * Main
 * ------------------------------------------------------------------------- */
int32_t main(void)
{
    /// STUDENTS: To be programmed




    /// END: To be programmed

    /* enable clock on GPIOA and GPIOB */
    GPIOA_ENABLE();
    GPIOB_ENABLE();

    disp_init();
    inout_init_io();
    
    while(1) {
        static uint8_t old_value = 0;
        uint8_t display_value = 0;
        
        uint8_t switches = inout_switch_read();
        
        inout_led_write(switches);
        
        switch (switches) {
            case 0x00:
                display_value = scan_matrix_keypad_cbc();
                break;
            case 0x01:
            case 0x02:
                display_value = scan_matrix_keypad_fast(0);
                break;
            case 0x03:
                display_value = scan_matrix_keypad_fast(1);
                break;
            default:
                display_value = switches;
                break;
        }
        
        if ((display_value != old_value) && (display_value != 0xFF)) {
            if (switches == 0x03) {
                disp_clear_buffer();
                if ((display_value & 0x10) == 0x10) {
                    disp_reg_new_value(0x1);
                }
            }
            
            disp_reg_new_value(display_value & 0x0F);
            disp_update();
        }
        
        old_value = display_value;
        
    }

    /// STUDENTS: To be programmed




    /// END: To be programmed
}

