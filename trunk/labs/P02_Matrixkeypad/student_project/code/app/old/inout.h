/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zurich University of                       -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 * --
 * -- Project     : CT2 Lab Matrix-Keypad
 * -- Description : Modul for using the mode switches (DIP_SWITCHES) and
 * --               the mode-leds
 * --
 * -- $Id: inout.h 213 2015-09-18 14:28:28Z scag $
 * ------------------------------------------------------------------------- */

/* re-definition guard */
#ifndef _INOUT_H
#define _INOUT_H


/* user includes */
#include "stdint.h"


/* makros -------------------------------------------------------------------*/


/* type definitions -------------------------------------------------------- */


/* public function declarations -------------------------------------------- */
/**
 *  \brief  Initialises the GPIOs used for the two DIP-Switches and LEDs
 *			must be called before using the DIP-Switches and LEDs
 */
void inout_init_io(void);

/**
 *  \brief  Reads the values of the dip-switches (called mode-keys)
 *  \return Value of the two switches (0..3)
 */
uint16_t inout_switch_read(void);

/**
 *  \brief  Writes current mode to the leds.
 *  \param  mode_nr: Number of the current mode (0..3).
 */
void inout_led_write(uint16_t mode_nr);
#endif


