/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zurich University of                       -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 * --
 * -- Project     : MC1 Lab Matrix-Keypad
 * -- Description : This code handles the reading of a 4x4
 * --               matrix-keypad (0..F)
 * --				The value of the pressed key will be displayed
 * --				on a 8-digit 7-segment-display.
 * --				For more information see description of the internship.
 * --
 * -- $Id: main.c 916 2016-10-10 13:21:27Z ruan $
 * ------------------------------------------------------------------------- */

/* includes ---------------------------------------------------------------- */
/// STDENTS: To be programmed
#include <stdint.h>
#include "hal_gpio.h"
#include "hal_rcc.h"
#include "keypad.h"
#include "display.h"
#include "inout.h"
/// END: To be programmed

/* -- defines----------------------------------------------------------------*/
/// STUDENTS: To be programmed


uint8_t disp_val;
uint16_t switch_val;

/// END: To be programmed


/* -- local function declarations -------------------------------------------*/


/* ----------------------------------------------------------------------------
 * Main
 * ------------------------------------------------------------------------- */
int32_t main(void)
{
    /// STUDENTS: To be programmed




    /// END: To be programmed

    /* enable clock on GPIOA and GPIOB */
    GPIOA_ENABLE();
    GPIOB_ENABLE();

    disp_init();
    inout_init_io();

    /// STUDENTS: To be programmed

		disp_clear_buffer();
	
		for(uint8_t i = 0; i<8; i++){
			disp_reg_new_value(i);
			disp_update();
		}
		
		disp_clear_buffer();
		disp_update();
	
		while(1){

			switch_val = inout_switch_read();
			inout_led_write(switch_val);
			
			if (switch_val == 0x0100) {
				disp_val = scan_matrix_keypad_cbc();
			}
			else if (switch_val == 0x0200) {
				disp_val = scan_matrix_keypad_fast(0x0);
			}
			
			
			if (disp_val != 0xFF){
				disp_reg_new_value(disp_val);
				disp_update();
			}
			
		}

    /// END: To be programmed
}

