/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zurich University of                       -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 * --
 * -- Project     : CT2 Lab Matrix-Keypad
 * -- Description : Modul handles the reading of the keypad
 * --
 * -- $Id: keypad.c 899 2016-09-27 06:31:38Z ruan $
 * ------------------------------------------------------------------------- */

#include "keypad.h"
#include "hal_gpio.h"
/* -- defines
 * --------------------------------------------------------------------------*/
/// STUDENTS: To be programmed

#define GPIO_matrixCol					(uint16_t)0x000F
#define GPIO_matrixRow					(uint16_t)0x00F0


/// END: To be programmed

/* type definitions -------------------------------------------------------- */
/// STUDENTS: To be programmed




/// END: To be programmed

/* internal function declarations ------------------------------------------ */

/// STUDENTS: To be programmed




/// END: To be programmed

/* internal variable definitions ------------------------------------------- */

const int key_array [4][4] = { { 0x1, 0x2, 0x3, 0xF },
                               { 0x4, 0x5, 0x6, 0xE },
                               { 0x7, 0x8, 0x9, 0xD },
                               { 0xA, 0x0, 0xB, 0xC } };

static uint8_t old_val;


/* public function definitions --------------------------------------------- */
/*
 * See header file
 */
uint8_t scan_matrix_keypad_cbc(void)
{
    uint8_t ret_val = NOKEY_PRESSED;
    /// STUDENTS: To be programmed
	
		hal_gpio_output_t portBCol_output = {GPIO_matrixCol, HAL_GPIO_PUPD_UP, HAL_GPIO_OUT_SPEED_2MHZ, HAL_GPIO_OUT_TYPE_PP};	
		hal_gpio_init_output(GPIOB, portBCol_output);
		
		hal_gpio_input_t portBRow_input = {GPIO_matrixRow, HAL_GPIO_PUPD_UP};
		hal_gpio_init_input(GPIOB, portBRow_input);
		
		
		hal_gpio_output_write(GPIOB,  GPIO_matrixCol | hal_gpio_input_read(GPIOB)); 
		
		uint8_t input_Data;
		uint8_t output_Data;
		uint8_t new_val = 0xFF;
		
		for (uint8_t i = 0; i<=3; i++)
		{
			output_Data = 0x1<<i;
			hal_gpio_bit_reset(GPIOB, output_Data);
			input_Data = ((~hal_gpio_input_read(GPIOB) & GPIO_matrixRow)>>4);
			
			if(input_Data != 0x0)
			{
				if (input_Data == 1){
					new_val = key_array [0][i];
				}else if (input_Data == 2){
					new_val = key_array [1][i];
				}else if (input_Data == 4){
					new_val = key_array [2][i];
				}else if (input_Data == 8){
					new_val = key_array [3][i];
				}
			}
			
			hal_gpio_output_write(GPIOB,  GPIO_matrixCol | hal_gpio_input_read(GPIOB)); 
			
		}
		
		if (new_val != old_val && new_val != 0xFF){
			ret_val = new_val;
		}

		old_val = new_val;

    /// END: To be programmed
    return ret_val;
}
/*
 * See header file
 */
uint8_t scan_matrix_keypad_fast(uint8_t use_shiftkey)
{
    uint8_t ret_val = NOKEY_PRESSED;
		uint8_t x;
		uint8_t y;
		uint8_t new_val = 0xFF;
    /// STUDENTS: To be programmed

		hal_gpio_output_t portBColOutput = {GPIO_matrixCol, HAL_GPIO_PUPD_UP, HAL_GPIO_OUT_SPEED_2MHZ, HAL_GPIO_OUT_TYPE_PP};	
		hal_gpio_init_output(GPIOB, portBColOutput);
		
		hal_gpio_input_t portBRowInput = {GPIO_matrixRow, HAL_GPIO_PUPD_UP};
		hal_gpio_init_input(GPIOB, portBRowInput);
		
		hal_gpio_output_write(GPIOB,  ~GPIO_matrixCol & hal_gpio_input_read(GPIOB)); 
		
		uint8_t scanRow = ~hal_gpio_input_read(GPIOB) & GPIO_matrixRow;
		
		
		hal_gpio_output_write(GPIOB, GPIO_matrixRow | GPIO_matrixCol | hal_gpio_input_read(GPIOB)); 
		
		hal_gpio_output_t portBRowOutput = {GPIO_matrixRow, HAL_GPIO_PUPD_UP, HAL_GPIO_OUT_SPEED_2MHZ, HAL_GPIO_OUT_TYPE_PP};	
		hal_gpio_init_output(GPIOB, portBRowOutput);
		
		hal_gpio_input_t portBColInput = {GPIO_matrixCol, HAL_GPIO_PUPD_UP};
		hal_gpio_init_input(GPIOB, portBColInput);
		
		hal_gpio_output_write(GPIOB, ~GPIO_matrixRow & hal_gpio_input_read(GPIOB)); 
		
		uint8_t scanCol = ~hal_gpio_input_read(GPIOB) & GPIO_matrixCol;
		
		scanRow = (scanRow>>4);
	
		if (scanCol != 0x00 & scanRow != 0x00){
			if (scanCol == 0x01){
				x = 0;				
			}else if(scanCol == 0x02){
				x = 1;	
			}else if(scanCol == 0x04){
				x = 2;		
			}else {
				x = 3;	
			}
			
			if (scanRow == 0x01){
				y = 0;				
			}else if(scanRow == 0x02){
				y = 1;	
			}else if(scanRow == 0x04){
				y = 2;		
			}else {
				y = 3;	
				
				
			}
			
			
			new_val = key_array [y][x];
			
		}
		
		if (old_val != new_val && new_val != 0xFF){
				ret_val = new_val;
			}
		
		old_val = new_val;

    /// END: To be programmed
    return ret_val;
		
		
}
/* internal functions definitions ------------------------------------------ */
/// STUDENTS: To be programmed




/// END: To be programmed

