/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zurich University of                       -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 * --
 * -- Project     : CT2 Lab Matrix-Keypad
 * -- Description : Modul for using the mode switches (DIP_SWITCHES) and
 * --               the mode-leds
 * --
 * -- $Id: inout.c 898 2016-09-27 06:13:57Z ruan $
 * ------------------------------------------------------------------------- */

#include "inout.h"
#include "hal_gpio.h"

/* defines ------------------------------------------------------------------*/
/// STUDENTS: To be programmed


#define GPIO_switchBoard				(uint16_t)0x0300
#define GPIO_ledBoard						(uint16_t)0x0C00

/// END: To be programmed
/* internal function declarations ------------------------------------------ */


/* internal variable definitions ------------------------------------------- */


/* public function definitions --------------------------------------------- */
/*
 * See header file
 */
void inout_init_io(void)
{
    /// STUDENTS: To be programmed

		hal_gpio_output_t portBDef_output = {GPIO_ledBoard, HAL_GPIO_PUPD_NOPULL, HAL_GPIO_OUT_SPEED_2MHZ, HAL_GPIO_OUT_TYPE_PP};
		hal_gpio_init_output(GPIOB, portBDef_output);
		
		hal_gpio_input_t portBDef_input = {GPIO_switchBoard, HAL_GPIO_PUPD_NOPULL};
		hal_gpio_init_input(GPIOB, portBDef_input);


    /// END: To be programmed
}
/*
 * See header file
 */
uint16_t inout_switch_read(void)
{
    uint16_t input = 0;
    /// STUDENTS: To be programmed

		input = hal_gpio_input_read(GPIOB) & GPIO_switchBoard;
	


    /// END: To be programmed
    return (uint16_t)input;
}

/*
 * See header file
 */
void inout_led_write(uint16_t mode_nr)
{
    /// STUDENTS: To be programmed

		hal_gpio_output_write(GPIOB,  (mode_nr<<2)| 0x03FF); 


    /// END: To be programmed
}

/* internal functions definitions ------------------------------------------ */
/// STUDENTS: To be programmed


