/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zurich University of                       -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 * --
 * -- Project     : CT2 Lab Matrix-Keypad
 * -- Description : Modul handles the 8-digit 7-segment-display
 * --				Controls the buffer (data for each digit)
 * --				Updates the display with the stored values
 * --
 * -- $Id: display.c 898 2016-09-27 06:13:57Z ruan $
 * ------------------------------------------------------------------------- */

/* -- includes
 * -------------------------------------------------------------------------*/
#include "display.h"
#include "hal_gpio.h"

/* -- defines
 * --------------------------------------------------------------------------*/
#define NUM_OF_DIGITS  8            /**< Number of digits                    */
#define DIGIT_BLANK    0xFF         /**< value to be written for blank digit */
/// STUDENTS: To be programmed

#define GPIO_sevenSeg			(uint16_t)0x00FF
#define GPIO_sevenAdr			(uint16_t)0x0700
#define GPIO_sevenEn			(uint16_t)0x0800
#define GPIO_all					(uint16_t)0x0FFF


/// END: To be programmed

/* internal function declarations ------------------------------------------ */
/// STUDENTS: To be programmed




/// END: To be programmed
/* internal variable definitions ------------------------------------------- */

const uint8_t int2hex [16] = { 0xC0,        //0
                               0xf9,        //1
                               0xa4,        //2
                               0xb0,        //3
                               0x99,        //4
                               0x92,        //5
                               0x82,        //6
                               0xf8,        //7
                               0x80,        //8
                               0x90,        //9
                               0x88,        //A
                               0x83,        //b
                               0xC6,        //C
                               0xA1,        //D
                               0x86,        //E
                               0x8E };      //F

static uint8_t disp_buf[NUM_OF_DIGITS];

/* public function definitions --------------------------------------------- */

/*
 * See header file
 */
void disp_reg_new_value(uint8_t value)
{
    /// STUDENTS: To be programmed

		for (uint8_t i = NUM_OF_DIGITS-1; i>0; i--) {
			disp_buf[i] = disp_buf[i-1];
		}
		
		disp_buf[0] = int2hex[value];


    /// END: To be programmed
}

/*
 * See header file
 */
void disp_update(void)
{
    /// STUDENTS: To be programmed

	
		for (uint8_t i = NUM_OF_DIGITS; i>0; i--) {
			hal_gpio_output_write(GPIOA, disp_buf[i-1] | ((i-1)<<8) | (0<<11));
			hal_gpio_bit_set(GPIOA, GPIO_sevenEn);
		}
		

    /// END: To be programmed
}
/*
 * See header file
 */
void disp_init(void)
{
    /// STUDENTS: To be programmed

		hal_gpio_output_t portADef = {GPIO_all, HAL_GPIO_PUPD_NOPULL, HAL_GPIO_OUT_SPEED_2MHZ, HAL_GPIO_OUT_TYPE_PP};
		
		hal_gpio_init_output(GPIOA, portADef);

    /// END: To be programmed
}

/*
 * See header file
 */
void disp_clear_buffer(void)
{
    /// STUDENTS: To be programmed

//////		for (uint8_t i = 0; i<NUM_OF_DIGITS; i++) {
//////			hal_gpio_output_write(GPIOA, DIGIT_BLANK | (i<<8));
//////		}
		
		
		for (uint8_t i = NUM_OF_DIGITS; i>0; i--) {
			disp_buf[i-1] = DIGIT_BLANK;
		}

    /// END: To be programmed
}

/// STUDENTS: To be programmed




/// END: To be programmed
/* internal functions definitions ------------------------------------------ */

/// STUDENTS: To be programmed




/// END: To be programmed

