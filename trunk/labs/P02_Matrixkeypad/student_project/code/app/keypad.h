/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zurich University of                       -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 * --
 * -- Project     : CT2 Lab Matrix-Keypad
 * -- Description : Modul handles the reading of the keypad
 * --
 * -- $Id: keypad.h 203 2015-09-17 11:41:45Z scag $
 * ------------------------------------------------------------------------- */

/* re-definition guard */
#ifndef _KEYPAD_H
#define _KEYPAD_H

/* user includes */
#include "stdint.h"

/* makros
 *---------------------------------------------------------------------------*/
#define NOKEY_PRESSED 0xFF


/* type definitions -------------------------------------------------------- */


/* public function declarations -------------------------------------------- */
/// STUDENTS: To be programmed

/* Scanning column by column */
uint8_t scan_matrix_keypad_cbc(void);

/* Fast scanning */
uint8_t scan_matrix_keypad_fast(uint8_t use_shiftkey);

/// END: To be programmed
#endif

