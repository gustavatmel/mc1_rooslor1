/* ----------------------------------------------------------------------------
 * --  _____       ______  _____                                              -
 * -- |_   _|     |  ____|/ ____|                                             -
 * --   | |  _ __ | |__  | (___    Institute of Embedded Systems              -
 * --   | | | '_ \|  __|  \___ \   Zurich University of                       -
 * --  _| |_| | | | |____ ____) |  Applied Sciences                           -
 * -- |_____|_| |_|______|_____/   8401 Winterthur, Switzerland               -
 * ----------------------------------------------------------------------------
 * --
 * -- Project     : CT2 Lab Matrix-Keypad
 * -- Description : Modul for using the mode switches (DIP_SWITCHES) and
 * --               the mode-leds
 * --
 * -- $Id: inout.c 898 2016-09-27 06:13:57Z ruan $
 * ------------------------------------------------------------------------- */

#include "inout.h"
#include "hal_gpio.h"

/* defines ------------------------------------------------------------------*/
/// STUDENTS: To be programmed
#define GPIO_SWITCH (uint16_t) 0x300
#define GPIO_LEDS   (uint16_t) 0xC00

#define OUTPUT_MASK (uint16_t) 0x3FF


/// END: To be programmed
/* internal function declarations ------------------------------------------ */


/* internal variable definitions ------------------------------------------- */


/* public function definitions --------------------------------------------- */
/*
 * See header file
 */
void inout_init_io(void)
{
    /// STUDENTS: To be programmed
    // Initialize dip switches as input, LEDs as output
    hal_gpio_input_t gpio_dip_switch = {
        GPIO_SWITCH,
        HAL_GPIO_PUPD_NOPULL // may need to be UP
    };
    hal_gpio_output_t gpio_leds = {
        GPIO_LEDS,
        HAL_GPIO_PUPD_NOPULL,
        HAL_GPIO_OUT_SPEED_2MHZ,
        HAL_GPIO_OUT_TYPE_PP
    };
    
    
    hal_gpio_init_input(GPIOB, gpio_dip_switch);
    hal_gpio_init_output(GPIOB, gpio_leds);
    /// END: To be programmed
}
/*
 * See header file
 */
uint8_t inout_switch_read(void)
{
    uint16_t input = 0;
    /// STUDENTS: To be programmed
    input = hal_gpio_input_read(GPIOB) & GPIO_SWITCH;
    input = input >> 8;
    /// END: To be programmed
    return (uint8_t)input;
}

/*
 * See header file
 */
void inout_led_write(uint8_t mode_nr)
{
    /// STUDENTS: To be programmed
    uint16_t output = hal_gpio_output_read(GPIOB) & OUTPUT_MASK;
    uint16_t switch_mask = 0;
    
    switch(mode_nr) {
        case 0x01:
            switch_mask = 0x400;
            break;
        case 0x02:
            switch_mask = 0x800;
            break;
        case 0x03:
            switch_mask = 0xC00;
            break;
        default: break;
    }
    
    output |= switch_mask;
    hal_gpio_output_write(GPIOB, output);
    /// END: To be programmed
}

/* internal functions definitions ------------------------------------------ */
/// STUDENTS: To be programmed




/// END: To be programmed


